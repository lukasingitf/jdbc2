/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc.service;

import java.sql.Connection;
import java.sql.SQLException;
import jdbc2.Customer;
import jdbc2.dao.CustomerDao;
import jdbc2.dao.ResourcesManager;
import jdbc2.exception.ShopException;

/**
 *
 * @author Uros
 */
public class CustomerService {
    
     private static final CustomerService instance = new CustomerService();

    private CustomerService() {
    }

    public static CustomerService getInstance() {
        return instance;
    }
    
    public void addNewCustomer (Customer customer) throws ShopException{
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            CustomerDao.getInstance().insert(customer, con);
            con.commit();
        } catch (SQLException ex) { 
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Ne moze se dodati customer!");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public Customer find(String username) throws ShopException{
        Connection con = null;
        try{
            con = ResourcesManager.getConnection();
            return CustomerDao.getInstance().find(username, con);
        } catch (SQLException ex) {
            throw new ShopException("Ne moze se naci customer sa tim username.");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void delete(String username) throws ShopException{
        Connection con  = null;
        try{
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            Customer customer = CustomerDao.getInstance().find(username, con);
            if(customer!=null){
              CustomerDao.getInstance().delete(customer, con);  
            }
            con.commit();
            
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Ne moze se obrisati customer");
        }finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void update(Customer customer) throws ShopException{
        Connection con = null;
        try{
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            CustomerDao.getInstance().update(customer, con);
            con.commit();
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Ne meze update customer");
        } finally{
            ResourcesManager.closeConnection(con);
        }
    }
    
}
