/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc.service;

import java.sql.Connection;
import java.sql.SQLException;
import jdbc2.Customer;
import jdbc2.Product;
import jdbc2.Purchase;
import jdbc2.dao.CustomerDao;
import jdbc2.dao.ProductDao;
import jdbc2.dao.PurchaseDao;
import jdbc2.dao.ResourcesManager;
import jdbc2.exception.ShopException;

/**
 *
 * @author Uros
 */
public class PurchaseService {
    
     private static final PurchaseService instance = new PurchaseService();
    
    private PurchaseService() {}
    
    public static PurchaseService getInstance() {
        return instance;
    }
    
    public void makePucrhase(Customer customer, Product product) throws ShopException{
        Connection con = null;
        try{
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            
            if(product.getCount()==0){
                throw new ShopException("Product nije dostupan");
            }
            
            if(product.getPrice()>customer.getCredit()){
                throw new ShopException("Nemate dovoljno para za kupovinu");
            }
            
            double credit = customer.getCredit() - product.getPrice();
            
            customer.setCredit(credit);
            CustomerDao.getInstance().update(customer, con);
            
            product.setCount(product.getCount()-1);
            ProductDao.getInstance().update(product, con);
            
            Purchase purchase= new Purchase(customer, product);
            PurchaseDao.getInstance().insert(purchase, con);
            
            con.commit();
            
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Ne moze se napraviti kupovina");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
}
