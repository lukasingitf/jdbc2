/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc.service;

import java.sql.Connection;
import java.sql.SQLException;
import jdbc2.Category;
import jdbc2.Product;
import jdbc2.dao.CategoryDao;
import jdbc2.dao.ProductDao;
import jdbc2.dao.ResourcesManager;
import jdbc2.exception.ShopException;

/**
 *
 * @author Uros
 */
public class ProductServise {
    
    private static ProductServise instance=null;

    public ProductServise() {
    }
    
    public static ProductServise getInstance(){
        if(instance == null){
            instance= new ProductServise();
        }
        return instance;
    }
    
    public int addNewProduct (Product p) throws ShopException{
        Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            return ProductDao.getInstance().insert(p, con);
            
            
        } catch (SQLException ex) {
            throw new ShopException("Ne moze se dodati novi product");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void addNewproductCategory(Category c) throws ShopException{
        Connection con = null;
        
        try{
            con = ResourcesManager.getConnection();
            CategoryDao.getInstance().insert(c, con);
        }catch (SQLException e){
            throw new ShopException("Ne moze se dodati category");
        }  finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public Product findProduct(String name) throws ShopException{
        Connection con= null;
        try{
            con = ResourcesManager.getConnection();
            return ProductDao.getInstance().find(name, con);
            
        } catch (SQLException ex) {
           throw new ShopException("Ne moze se pronaci product");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    
    public Category findCategory (String name) throws ShopException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            return CategoryDao.getInstance().find(name, con);
        } catch (SQLException ex) {
            throw new ShopException("Ne moze se pronaci category");
        }
    }
    
    public void update(Product p) throws ShopException {
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
            ProductDao.getInstance().update(p, con);
            con.commit();
            
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Ne moze se update-ovati product");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
    public void deleteProduct (String productName) throws ShopException{
        Connection con = null;
        try {
            con = ResourcesManager.getConnection();
            con.setAutoCommit(false);
           Product p = ProductDao.getInstance().find(productName, con);
           if (p!=null){
                ProductDao.getInstance().delete(p, con);
           }
           con.commit();
                   
               
        } catch (SQLException ex) {
            ResourcesManager.rollbackTransactions(con);
            throw new ShopException("Nije moguce obrisati product sa zadatim imenom");
        } finally {
            ResourcesManager.closeConnection(con);
        }
    }
    
}
