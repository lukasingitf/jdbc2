/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

/**
 *
 * @author Uros
 */
public class Purchase {
    private Customer customer;
    private int idPurchase;
    private Product product;

    public Purchase(Customer customer, Product product) {
        this.customer = customer;
        this.product = product;
    }
    
    
    public Purchase(Customer customer, int idPurchase, Product product) {
        this.customer = customer;
        this.idPurchase = idPurchase;
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getIdPurchase() {
        return idPurchase;
    }

    public void setIdPurchase(int idPurchase) {
        this.idPurchase = idPurchase;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Purchase{" + "customer=" + customer + ", idPurchase=" + idPurchase + ", product=" + product + '}';
    }
    
    
}
