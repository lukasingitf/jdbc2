/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

/**
 *
 * @author Uros
 */
public class Adress {
    private int idAddress=-1;
    private String city;
    private String street;
    private String streetNumber;

    public Adress(String city, String street, String streetNumber) {
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
    }
    public Adress(int idAddress, String city, String street, String streetNumber) {
        this.idAddress = idAddress;
        this.city = city;
        this.street = street;
        this.streetNumber = streetNumber;
    }

    public int getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(int idAddress) {
        this.idAddress = idAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Override
    public String toString() {
        return "Adress{" + "idAddress=" + idAddress + ", city=" + city + ", street=" + street + ", streetNumber=" + streetNumber + '}';
    }
    
}

