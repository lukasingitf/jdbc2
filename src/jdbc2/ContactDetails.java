/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2;

/**
 *
 * @author Uros
 */
public class ContactDetails {
    private int idContactDetails;
    private String emailAdress;
    private String phoneNumber;

    public ContactDetails(String emailAdress, String phoneNumber) {
        this.emailAdress = emailAdress;
        this.phoneNumber = phoneNumber;
    }
    
    
    public ContactDetails(int idContactDetails, String emailAdress, String phoneNumber) {
        this.idContactDetails = idContactDetails;
        this.emailAdress = emailAdress;
        this.phoneNumber = phoneNumber;
    }

    public int getIdContactDetails() {
        return idContactDetails;
    }

    public void setIdContactDetails(int idContactDetails) {
        this.idContactDetails = idContactDetails;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Contact_details{" + "idContactDetails=" + idContactDetails + ", emailAdress=" + emailAdress + ", phoneNumber=" + phoneNumber + '}';
    }
    
    
    
}
