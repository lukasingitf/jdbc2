/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.main;


import jdbc2.Adress;
import jdbc2.Category;
import jdbc2.ContactDetails;
import jdbc2.Customer;
import jdbc2.Product;
import jdbc2.exception.ShopException;
import jdbc.service.CustomerService;
import jdbc.service.ProductServise;
import jdbc.service.PurchaseService;

public class Start {

   private static final ProductServise productService = ProductServise.getInstance();
    private static final CustomerService customerService = CustomerService.getInstance();
    private static final PurchaseService purchaseService = PurchaseService.getInstance();
    
    
    public static void main(String[] args) throws ShopException {
        addCategory();
        addCustomers();
        addProduct();
    }
    
    public static void addCustomers() throws ShopException{
        customerService.addNewCustomer(new Customer("pera", "Pera", "Peric", 30000, new ContactDetails("pera@gmail.com", "0112312313"), new Adress("Beograd", "Kumadraska", "2")));
    }
    
    public static void addProduct() throws ShopException{
        productService.addNewProduct(new Product("Lego kockice", 8000, 5, productService.findCategory("Igracke")));
    }
    
    public static void addCategory() throws ShopException{
        productService.addNewproductCategory(new Category("Igracke"));
    }
    
    public static void makePurchase() throws ShopException{
        purchaseService.makePucrhase(customerService.find("pera"), productService.findProduct("Lego kockice"));
    }
}
