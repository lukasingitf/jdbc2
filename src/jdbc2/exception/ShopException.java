 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.exception;

/**
 *
 * @author Uros
 */
public class ShopException extends Exception {

    public ShopException(String message) {
        super(message);
    }
    public ShopException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
