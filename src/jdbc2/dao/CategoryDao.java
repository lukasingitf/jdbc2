/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import com.sun.beans.util.Cache;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc2.Category;

/**
 *
 * @author Uros
 */
public class CategoryDao {
    
    private static final CategoryDao instance = new CategoryDao();

    private CategoryDao() {
    }

    public static CategoryDao getInstance() {
        return instance;
    }
    
    public Category find(int idCategory, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Category category = null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM category where id_category=?");
            ps.setInt(1, idCategory);
            rs = ps.executeQuery();
            if (rs.next()){
                category = new Category(idCategory, rs.getString("name"));
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return category;
    }
    
     public Category find(String name, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Category category = null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM category where id_category=?");
           ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()){
                category = new Category(rs.getInt("id_category"), name);
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return category;
    }
    
    public void insert(Category category, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps=con.prepareStatement("INSERT INTO category where(name) VALUES(?)");
            ps.setString(1, category.getName());
            ps.executeUpdate();
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
        
    }
    
    public void update(Category category, Connection con) throws SQLException{
        PreparedStatement ps  = null;
        try{
            ps=con.prepareStatement("UPDATE category SET name=? WHERE id_category=?");
            ps.setString(1, category.getName());
            ps.setInt(2, category.getIdCategory());
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete (String name, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps= con.prepareStatement("DELETE category WHERE name=?");
            ps.setString(1, name);
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete (int idCategory, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps= con.prepareStatement("DELETE category WHERE name=?");
            ps.setInt(1, idCategory);
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
}


