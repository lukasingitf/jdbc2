/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import jdbc2.ContactDetails;

/**
 *
 * @author Uros
 */
public class ContactDetailsDao {
    
     private static final ContactDetailsDao instance = new ContactDetailsDao();

    private ContactDetailsDao() {
    }

    public static ContactDetailsDao getInstance() {
        return instance;
    }
    
    public ContactDetails find(int idContactDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ContactDetails contactDetails = null;
        try {
            ps = con.prepareStatement("SELECT * FROM contact_details where id_contact_details=?");
            ps.setInt(1, idContactDetails);
            rs = ps.executeQuery();
            if (rs.next()) {
                contactDetails = new ContactDetails(idContactDetails, rs.getString("email_address"), rs.getString("phone_number"));
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return contactDetails;
    }
    
    public int insert(ContactDetails contactDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id = -1;
        try {
            ps = con.prepareStatement("INSERT INTO contact_details(email_address, phone_number) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, contactDetails.getEmailAdress());
            ps.setString(2, contactDetails.getPhoneNumber());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id = rs.getInt(1);
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(ContactDetails contactDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("UPDATE contact_details SET email_address=?, phone_number=? WHERE id_contact_details=?");
            ps.setString(1, contactDetails.getEmailAdress());
            ps.setString(2, contactDetails.getPhoneNumber());
            ps.setInt(3, contactDetails.getIdContactDetails());
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete(int idContactDetails, Connection con) throws SQLException {
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE FROM contact_details WHERE id_contact_details=?");
            ps.setInt(1, idContactDetails);
            ps.executeUpdate();
        } finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
}
