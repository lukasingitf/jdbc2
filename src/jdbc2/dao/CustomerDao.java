/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdbc2.Adress;
import jdbc2.ContactDetails;
import jdbc2.Customer;

/**
 *
 * @author Uros
 */
public class CustomerDao {
    
    private static final CustomerDao instance = new CustomerDao();

    private CustomerDao() {
    }

    public static CustomerDao getInstance() {
        return instance;
    }
    
    public Customer find(String username, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Customer customer=null;
        try {
            ps= con.prepareStatement("SELECT * FROM customer WHERE username=?");
            ps.setString(1, username);
            rs=ps.executeQuery();
            if(rs.next()) {
                Adress adress= AdressDao.getInstance().find(rs.getInt("fk_address"), con);
                ContactDetails contactDetails = ContactDetailsDao.getInstance().find(rs.getInt("fk_contact_details"), con);
                customer = new Customer(rs.getString("username"), rs.getString("name"), rs.getString("surname"),
                        rs.getDouble("credit"), contactDetails, adress);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return customer; 
    }
    
    public void insert(Customer customer, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            Integer fkAddress = null;
            //ovde insertujemo adresu  i vraca nam se id
            if(customer.getAddress() !=null){
                fkAddress = AdressDao.getInstance().insert(customer.getAddress(), con);
            }
            
            Integer fkContactDetails=null;
            if(customer.getContactDetails() != null){
                fkContactDetails = ContactDetailsDao.getInstance().insert(customer.getContactDetails(),con);
            }
            
            ps= con.prepareStatement("INSERT INTO customer(username, name, surname, credit, fk_contact_details, fk_address) VALUES(?,?,?,?,?,?)");
            ps.setString(1, customer.getUsername());
            ps.setString(2, customer.getName());
            ps.setString(3, customer.getSurname());
            ps.setDouble(4, customer.getCredit());
            ps.setInt(5, fkContactDetails);
            ps.setInt(6, fkAddress);
            ps.executeUpdate();
            
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void update (Customer customer,Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE customer SET name=?, surname=?, credit=? WHERE username=?");
            ps.setString(1, customer.getName());
            ps.setString(2, customer.getSurname());
            ps.setDouble(3, customer.getCredit());
            ps.setString(4, customer.getUsername());
            ps.executeUpdate();
            
            if(customer.getAddress()!=null){
                AdressDao.getInstance().update(customer.getAddress(), con);
            }
            
            if(customer.getContactDetails()!=null){
                ContactDetailsDao.getInstance().update(customer.getContactDetails(), con);
            }
            
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    
    public void delete(Customer customer, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            //brisanje purchase
       //     PurchaseDao.getInstance().delete(customer, con);
       
            //brisanje adrese
            if(customer.getAddress()!=null){
                AdressDao.getInstance().delete(customer.getAddress().getIdAddress(), con);
                
            }
            
            if(customer.getContactDetails()!=null){
                ContactDetailsDao.getInstance().delete(customer.getContactDetails().getIdContactDetails(), con);
            }
            
       
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
}
