/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import jdbc2.Customer;
import jdbc2.Product;
import jdbc2.Purchase;


/**
 *
 * @author Uros
 */
public class PurchaseDao {
     private static PurchaseDao instance=null;

    public PurchaseDao() {
    }
    
    public static PurchaseDao getInstance(){
        if(instance == null){
            instance= new PurchaseDao();
        }
        return instance;
    }
    
    public Purchase find(int idPurchase, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Purchase purchase= null;
        try{
            ps= con.prepareStatement("SELECT * FROM purchase WHERE id_purchase=?");
            ps.setInt(1, idPurchase);
            rs= ps.executeQuery();
            if(rs.next()){
                Customer customer= CustomerDao.getInstance().find(rs.getString("fk_customer"), con);
                Product product = ProductDao.getInstance().find(rs.getInt("fk_product"), con);
                purchase = new Purchase(customer, idPurchase, product);
            }
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return purchase;
    }
    
    public int insert(Purchase purchase, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO purchase (fk_customer, fk_product) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, purchase.getCustomer().getUsername());
            ps.setInt(2, purchase.getProduct().getIdProduct());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id=rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void delete (Product product, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM purchase WHERE fk_product=?");
             ps.setInt(1, product.getIdProduct());
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
    
     public void delete (Customer customer, Connection con) throws SQLException{
         PreparedStatement ps = null;
         try{
             ps= con.prepareStatement("DELETE FROM purchase WHERE fk_customer=?");
             ps.setString(1, customer.getUsername());
             ps.executeUpdate();
         } finally {
             ResourcesManager.closeResources(null, ps);
         }
    }
    
    
    
    
   
    
}
