/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import jdbc2.Adress;

/**
 *
 * @author Uros
 */
public class AdressDao {
    private static AdressDao instance=null;

    public AdressDao() {
    }
    
    public static AdressDao getInstance(){
        if(instance == null){
            instance= new AdressDao();
        }
        return instance;
    }
 
    protected Adress find(int idAdress, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Adress adress=null;
        
        try {
            ps= con.prepareStatement("SELECT * FROM address WHERE id_address=?");
            ps.setInt(1, idAdress);
            rs=ps.executeQuery();
            if(rs.next()){
                adress= new Adress(idAdress, rs.getString("city"), rs.getString("street"), rs.getString("streetNumber"));
            }
        }
        finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return adress;
        
    }
    
    
    protected int insert(Adress adress, Connection con) throws SQLException {
        PreparedStatement ps= null;
        ResultSet rs= null;
        int id=-1;
        try{
            ps= con.prepareStatement("INSERT INTO address(city, street, street_number) VALUE (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, adress.getCity());
            ps.setString(2, adress.getStreet());
            ps.setString(3, adress.getStreetNumber());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id= rs.getInt(1);
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    public void update(Adress adress, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps = con.prepareStatement("UPDATE address SET city=?, street=?, street_number=? WHERE id_address=?");
            ps.setString(1, adress.getCity());
            ps.setString(2, adress.getStreet());
            ps.setString(3, adress.getStreetNumber());
            ps.setInt(4, adress.getIdAddress());
            ps.executeUpdate();
        } finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    
    public void delete(int idAdress, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("DELETE address WHERE id_address=?");
            ps.setInt(1, idAdress);
            ps.executeUpdate();
        }finally{
            ResourcesManager.closeResources(null, ps);
        }
    }
}
