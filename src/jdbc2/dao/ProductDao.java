/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import jdbc2.Category;
import jdbc2.Product;
import jdbc2.exception.ShopException;

/**
 *
 * @author Uros
 */
public class ProductDao {
    
    private static final ProductDao instance = new ProductDao();

    private ProductDao() {
    }

    public static ProductDao getInstance() {
        return instance;
    }
    
    public Product find(String name, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Product product= null;
        try {
            ps= con.prepareStatement("SELECT Product WHERE name=?");
            ps.setString(1, name);
            rs = ps.executeQuery();
            if(rs.next()){
                Category category = CategoryDao.getInstance().find(rs.getInt("fk_category"), con);
                product = new Product(name,rs.getInt("id_product"), rs.getDouble("price"), rs.getInt("count"), category);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return product;
    }
    
    public Product find(int idProduct, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Product product= null;
        try {
            ps= con.prepareStatement("SELECT Product WHERE name=?");
            ps.setInt(1, idProduct);
            rs = ps.executeQuery();
            if(rs.next()){
                Category category = CategoryDao.getInstance().find(rs.getInt("fk_category"), con);
                product = new Product(rs.getString("name"),rs.getInt("id_product"), rs.getDouble("price"), rs.getInt("count"), category);
            }
        } finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return product;
    }
    
    public List<Product> findAll(Category category, Connection con) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Product> productList= new ArrayList<>();
        try{
            ps = con.prepareStatement("SELECT * FROM product WHERE fk_category=?");
            ps.setInt(1, category.getIdCategory());
            rs = ps.executeQuery();
            while(rs.next()){
                Product product = new Product(rs.getString("name"), rs.getDouble("price"), rs.getInt("count"), category);
                productList.add(product);
            }  
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return productList;
    }
    
    public int insert(Product product, Connection con) throws SQLException, ShopException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        int id=-1;
        try {
            ps = con.prepareStatement("INSERT INTO product(name, price, count, fk_category) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, product.getName());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getCount());
            Category category=CategoryDao.getInstance().find(product.getCategory().getName(), con);
            if(category == null) {
                throw new ShopException("Category "+ product.getCategory()+" ne postoji.");
                
            }
            ps.setInt(4, category.getIdCategory());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            rs.next();
            id=rs.getInt(1);
            
        }finally {
            ResourcesManager.closeResources(rs, ps);
        }
        return id;
    }
    
    
    public void update(Product product, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            ps= con.prepareStatement("UPDATE product SET name=?, price=?, count=?, fk_category=?, WHERE id_product=?");
            ps.setString(1, product.getName());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getCount());
            ps.setInt(4, product.getCategory().getIdCategory());
            ps.setInt(5, product.getIdProduct());
            ps.executeUpdate();
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    public void delete (Product product, Connection con) throws SQLException{
        PreparedStatement ps = null;
        try{
            
            //delete purchases of the product
            //PurchaseDao.getInstance().delete(product, con);

            //delete product
            
            ps = con. prepareStatement("DELETE product WHERE id_product=?");
            ps.setInt(1, product.getIdProduct());
            ps.executeUpdate();
            
        }finally {
            ResourcesManager.closeResources(null, ps);
        }
    }
    
    
}
